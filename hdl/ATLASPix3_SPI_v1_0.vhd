library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity ATLASPix3_SPI_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
		spi_clk_p : out std_logic;
        spi_clk_n : out std_logic; 
        ss_p : out std_ulogic_vector(0 downto 0);
        ss_n : out std_ulogic_vector(0 downto 0);
        miso_p : in std_logic;
        miso_n : in std_logic; 
        mosi_p : out std_logic;
        mosi_n : out std_logic;
        debug : out std_logic_vector(31 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end ATLASPix3_SPI_v1_0;

architecture arch_imp of ATLASPix3_SPI_v1_0 is

	-- component declaration
	component ATLASPix3_SPI_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic;
		spi_clk : out std_logic;
        ss : out std_ulogic_vector(0 downto 0);
        miso : in std_logic;
        mosi : out std_logic;
        debug : out std_logic_vector(31 downto 0)  
		);
	end component ATLASPix3_SPI_v1_0_S00_AXI;

    signal miso : std_logic;
    signal mosi : std_logic;
    signal spi_clk : std_logic;
    signal ss: std_ulogic_vector(0 downto 0);
begin

-- Instantiation of Axi Bus Interface S00_AXI
ATLASPix3_SPI_v1_0_S00_AXI_inst : ATLASPix3_SPI_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready,
		spi_clk => spi_clk,
		miso => miso , 
		mosi => mosi, 
		ss => ss,
		debug => debug
	);

	-- Add user logic here
         IBUFDS_inst : IBUFDS
     generic map (
        DQS_BIAS => "FALSE"  -- (FALSE, TRUE)
     )
     port map (
        O => miso,   -- 1-bit output: Buffer output
        I => miso_p,   -- 1-bit input: Diff_p buffer input (connect directly to top-level port)
        IB => miso_n  -- 1-bit input: Diff_n buffer input (connect directly to top-level port)
     );
       OBUFDS_inst2 : OBUFDS
    generic map (
       IOSTANDARD => "DEFAULT", -- Specify the output I/O standard
       SLEW => "SLOW")          -- Specify the output slew rate
    port map (
       O => mosi_p,     -- Diff_p output (connect directly to top-level port)
       OB => mosi_n,   -- Diff_n output (connect directly to top-level port)
       I => mosi      -- Buffer input 
    );
       OBUFDS_inst3 : OBUFDS
    generic map (
       IOSTANDARD => "DEFAULT", -- Specify the output I/O standard
       SLEW => "SLOW")          -- Specify the output slew rate
    port map (
       O => spi_clk_p,     -- Diff_p output (connect directly to top-level port)
       OB => spi_clk_n,   -- Diff_n output (connect directly to top-level port)
       I => spi_clk      -- Buffer input 
    );
       OBUFDS_inst4 : OBUFDS
    generic map (
       IOSTANDARD => "DEFAULT", -- Specify the output I/O standard
       SLEW => "SLOW")          -- Specify the output slew rate
    port map (
       O => ss_p(0),     -- Diff_p output (connect directly to top-level port)
       OB => ss_n(0),   -- Diff_n output (connect directly to top-level port)
       I => ss(0)      -- Buffer input 
    );
	-- User logic ends

end arch_imp;
