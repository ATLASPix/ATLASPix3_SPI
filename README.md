# ATLASPix3 SPI interface

This IP interface the SPI control of ATLASPix3 to AXI registers. Transaction through SPI with ATLASPix3 are done via 88 bit words. Only the first 24 bits of the word are used. 

## Registers

- **Register 0** (0x0) : unused
- **Register 1** (0x4) SPI_DATA_IN,24 bits :  input data for transmission through SPI, fill the SPI fifo
- **Register 2** (0x8) SPI_WRITE, 1 bit : Empty the SPI FIFO through the SPI interface when 0x1 is asserted
- **Register 3** (0xC) RESET_FIFO, 1 bit : Reset for FIFO and its state-machine  
- **Register 4** (0x10) RESET_SPI 1 bit : Reset for SPI master block


## operation

When a write operation to SPI_DATA_IN is detected, the corresponding 88 bit word to be transmitted is written to the SPI FIFO. Up to 65k SPI word can be written to FIFO. When SPI_WRITE is asserted (0x1), the data words stored in FIFO are push via SPI to the ATLASPix3 until the FIFO is empty again. SPI_WRITE is then de-asserted automatically. 

The 24 bits of the SPI_DATA correspond to 24 control bits of the ATLASPix3 register. See ATLASPix3 documentation for the definition of these bits.